﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class comment_switcher : MonoBehaviour {

	public List<GameObject> comments;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void show(int input_level){
		foreach(GameObject comment in comments){
			comment.SetActive(false);
		}
		
		//gameObject.SetActive(false);

		int no = input_level % 3;

		comments [no].SetActive (true);
	}
}
