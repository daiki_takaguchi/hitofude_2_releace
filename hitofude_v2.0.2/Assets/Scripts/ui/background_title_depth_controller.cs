﻿using UnityEngine;
using System.Collections;

public class background_title_depth_controller : MonoBehaviour {

	// Use this for initialization

	public UIPanel panel_background;
	public UIPanel panel_button;

	public depth_controller background_depth;
	public depth_controller button_mode_normal_depth;
	public depth_controller button_mode_dan_depth;
	public depth_controller button_mode_normal_label_depth;
	public depth_controller button_mode_dan_label_depth;

	public Transform animation_position_background;
	public Transform animation_position_buttons;
	public Transform animation_position_buttons_label;


	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void change_layer(depth_controller.depth_layer back_depth, depth_controller.depth_layer button_depth,depth_controller.depth_layer label_depth, int input_layer_no){
		background_depth.change_layer (back_depth,input_layer_no);
		button_mode_normal_depth.change_layer (button_depth,input_layer_no);
		button_mode_dan_depth.change_layer (button_depth,input_layer_no);
		button_mode_normal_label_depth.change_layer (label_depth,input_layer_no);
		button_mode_dan_label_depth.change_layer (label_depth,input_layer_no);
		//Debug.Break();
	}

	public void change_layer_back(){
		//change_layer
		int temp_layer = gameObject.layer;

		//panel_background.gameObject.layer = temp_layer;
		//panel_button.gameObject.layer = temp_layer;
		change_layer(depth_controller.depth_layer.Background_1,depth_controller.depth_layer.Background_1_button,depth_controller.depth_layer.Background_1_Label,temp_layer);

	}

	public void change_layer_front(){

		int temp_layer = gameObject.layer;
		
		//panel_background.gameObject.layer = temp_layer;
		//panel_button.gameObject.layer = temp_layer;

		change_layer(depth_controller.depth_layer.Background_1_anim,depth_controller.depth_layer.Background_1_button_anim,depth_controller.depth_layer.Background_1_Label_anim,temp_layer);

	}

}
