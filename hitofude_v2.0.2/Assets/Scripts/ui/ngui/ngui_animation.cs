﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class ngui_animation : MonoBehaviour {

	public List<Texture> animation_sprites= new List<Texture>();

	public int current_sprite=0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Texture get_next_sprite(){

	

		if(current_sprite<animation_sprites.Count-1){
			current_sprite++;
		}
		//print (current_sprite);
		return animation_sprites[current_sprite];
	}

	public Texture get_first_sprite(){
		return animation_sprites [0];
	}
}
