﻿using UnityEngine;
using System.Collections;

public class ngui_screen_adjust : MonoBehaviour {

	// Use this for initialization

	public UI_controller ui;

	public enum object_type{
		normal_mode,
		dan_mode,
		large_label_level,
		small_label_level,
		large_label_dan,
		small_label_dan,
		result_button,
		applimotion,
		result_stars,
		turn_label_level,
		turn_label_dan,
		hitofude_touch_panel
	}

	public object_type type;

	void Start () {
		reposition ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void reposition(){

		Vector2 position = ui.hitofude_data.settings.object_reposition(type);

		Vector3 temp = transform.localPosition;

		transform.localPosition = new Vector3 (position.x,position.y,temp.z);

		if(type==object_type.result_stars){
			if(ui.hitofude_data.settings.get_screen_type()==hitofude_settings.screen_type_enum.ipad_3_4){
				transform.localScale=new Vector3(0.70f,0.70f,1);
			}

		}
		
	}
}
