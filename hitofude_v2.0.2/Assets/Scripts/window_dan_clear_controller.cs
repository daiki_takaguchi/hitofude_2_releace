﻿using UnityEngine;
using System.Collections;

public class window_dan_clear_controller : MonoBehaviour {

	// Use this for initialization

	public UILabel label; 

	public animation_state state;

	//private float wait_time=1.5f;

	public enum animation_state{
		cleared,
		time_out
	}

	void Start () {
	
	}


	
	// Update is called once per frame
	void Update () {
	
	}

	public void show_window(string input_label, animation_state input_state){

		set_text (input_label);
		state = input_state;

	}

	void set_text(string input){
		label.text = input;
	}

	IEnumerator wait_for_seconds(float time){
		yield return new WaitForSeconds(time);
		wait_end();
	}

	public void wait_end(){
		gameObject.SetActive (false);
	}
}
