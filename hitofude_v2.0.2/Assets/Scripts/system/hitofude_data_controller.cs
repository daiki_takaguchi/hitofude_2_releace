﻿using UnityEngine;
using System.Collections;

public class hitofude_data_controller : MonoBehaviour {

	// Use this for initialization

	//public bool clear_now =false;

	public hitofude_settings settings;
	/*
	public int max_level;
	public int max_dan_stage=5;
*/

	public hitofude_data_loader loader;
	public save_data_controller save_data;
	public UI_controller ui;

	public GameObject points_parent;
	public GameObject lines_parent;

	public GameObject point_prefab;
	public GameObject line_prefab;


	public hitofude_point_controller[] points;
	public hitofude_line_controller[] lines;
	public hitofude_line_controller[] answer_lines;

	public hitofude_point_controller[] selected_points;
	public hitofude_line_controller[] selected_lines;

	//public hitofude_point_controller last_point;
	public hitofude_line_controller dragged_line;


	public int[] answer;
	public bool[] quest;
	public int[] prev_data;

	public int undo_count=0;

	public bool is_dan;
	public bool is_hint_mode=false;
	public bool is_answer_mode = false;

	
	public int current_level;
	public int current_dan;

	public int[] current_dan_ints;

	public int current_dan_stage;

	public bool is_reflected_x=false;
	public bool is_reflected_y=false;
	public int rotation_angle=0;

	public bool is_cleared=false;

	void Start () {
		prev_data = new int[settings.max_dan_stage];
		dragged_line.set_width (this);

		dragged_line.change_color (settings.red);

		//dragged_line.change_state (hitofude_line_controller.line_state.selected);

		//int[] temp = new int[5] {1,2,3,4,5};
		//current_dan_ints = pick_random_ints (temp,5);

	}
	
	// Update is called once per frame
	void Update () {
		if(settings.clear_now&&lines.Length!=0&&!is_cleared){
			check_answer();

			settings.clear_now=false;

		}
	
	}

	  
	public void point_clicked(GameObject clicked){


		//hitofude_draw_controller.draw_line (clicked);


	}

	public void button_undo_pressed(){

		if (is_answer_mode) {

		} else if( selected_points.Length!=0){
			undo_one_line (true);
			refresh_point_availability();
		}
	}

	public void undo_one_line(bool input_count_undo){

		if(selected_points.Length!=0){

			selected_points[selected_points.Length-1].selected_count--;

			if(selected_points[selected_points.Length-1].selected_count<=0){
				selected_points[selected_points.Length-1].selected_count=0;
				selected_points[selected_points.Length-1].change_state(hitofude_point_controller.point_state.normal);
			}



			delete_selected_point();

			if(selected_lines.Length!=0){

				//last_point.change_state(hitofude_point_controller.point_state.normal);
				//selected_lines[selected_lines.Length-1]
				selected_lines[selected_lines.Length-1].undo();

				delete_selected_line();

			}

			if(input_count_undo){
				undo_count++;
			}

			if(is_hint_mode&&selected_points.Length==0){
				button_hint_yes_pressed();
			}


		}


	}

	public void button_restart_pressed(bool input_count_undo){

		if(is_answer_mode){

		} else if(selected_points.Length!=0){
			int temp = selected_lines.Length;

			for (int i=0;i<=temp;i++){
				undo_one_line(false);
			}
			if(input_count_undo){
				undo_count++;
			}

			refresh_point_availability ();

		}




	}

	public void delete_selected_point(){
		if (selected_points.Length != 0) {

			selected_points[selected_points.Length-1].enable_touch();

			hitofude_point_controller[] new_points = new hitofude_point_controller[selected_points.Length-1];

			for (int i=0; i<selected_points.Length-1; i++) {
				new_points[i]=selected_points[i];
			}

			
			selected_points=  new_points;

		}

	}
	

	public void delete_selected_line(){
		if (selected_lines.Length != 0) {
			hitofude_line_controller[] new_lines = new hitofude_line_controller[ selected_lines.Length-1];
			
			for (int i=0; i< selected_lines.Length-1; i++) {
				new_lines[i]=selected_lines[i];
			}

			selected_lines=new_lines;
		}
	}

	public hitofude_point_controller find_point_by_id(int input_id){

		hitofude_point_controller output = null;

		foreach (hitofude_point_controller temp in points){
			if(temp.id==input_id){
				output=temp;
			}
		}

		return output;
	}
	
	public void add_selected_point(hitofude_point_controller input_point){

		hitofude_point_controller[] new_points = new hitofude_point_controller[selected_points.Length+1];
		
		
		for (int i=0; i<selected_points.Length; i++) {
			new_points[i]=selected_points[i];
		}

		new_points[selected_points.Length]=input_point;
		
		selected_points=  new_points;
		
		selected_points [selected_points.Length-1].selected_count++;

		if(settings.ripple_current_point){
			input_point.point_animation_state=hitofude_point_controller.Point_animation_state.start;
		}

	}

	public void add_selected_line(hitofude_point_controller input_end_point){

		if(selected_points.Length>0){

			hitofude_line_controller[] new_lines = new hitofude_line_controller[ selected_lines.Length+1];

			for (int i=0; i< selected_lines.Length; i++) {
				new_lines[i]=selected_lines[i];
			}

			hitofude_point_controller input_start_point =selected_points[selected_points.Length-1];

			hitofude_line_controller new_line=input_start_point.search_line(input_end_point.id);

			new_line.selected();

			new_lines[selected_lines.Length]=new_line;
		
			selected_lines = new_lines;




		}


		
		add_selected_point (input_end_point);



		if(is_answer_mode&&selected_points.Length<answer.Length){
			//ui.answer_point.position.localPosition= find_point_by_id (answer[selected_points.Length]).transform.localPosition;
			ui.answer_point.set_position(find_point_by_id (answer[selected_points.Length]).transform.localPosition);
			ui.answer_point.label.SetActive(false);
		}

		if(is_hint_mode){
			ui.answer_point.animation_end();
			//disable_hint_mode();
		}

		refresh_point_availability ();

		hide_dragged_line ();

		check_answer ();
	}


	public void check_answer(){

		bool is_ok = false;



		if(answer.Length==selected_points.Length){
			is_ok=true;
		}

		if(is_ok||settings.clear_now){

			is_cleared=true;
			ui.SE.play_audio (SE_controller.SE_list.clear);

				if(is_dan){

					if(current_dan_stage<settings.max_dan_stage-1){

						ui.button_next_dan_stage_pressed();
					}else{

						save_data.set_select_dan_data(current_dan,ui.result_to_level_state(hitofude_result()));
						save_data.set_select_dan_data(current_dan+1,button_select_level_controller.level_state.Zero);
						ui.button_result_pressed();
					}

					
				}else{

					save_data.set_select_level_data(current_level,ui.result_to_level_state(hitofude_result()));
					save_data.set_select_level_data(current_level+1,button_select_level_controller.level_state.Zero);

					ui.button_result_pressed();
				}


		}


	}

	public void clear_data(){

		foreach (hitofude_point_controller temp in points) {
			Destroy(temp.parent);
		}

		points= new hitofude_point_controller[0];

		//temps=GameObject.FindGameObjectsWithTag ("hitofude_line");
		
		foreach (hitofude_line_controller temp in lines) {
			Destroy(temp.parent);
		}

		lines= new hitofude_line_controller[0];

		selected_lines = new hitofude_line_controller[0];

		//if(is_answer_mode|){

		hide_dragged_line ();
		ui.answer_point.animation_end();
		ui.scale_UI_play (new Vector2(0,0),1);
		//}

	}

	public window_result_controller.result hitofude_result(){
		window_result_controller.result output = window_result_controller.result.good;

		if(undo_count==0){
			output=window_result_controller.result.perfect;
		}else if(undo_count==1){
			output=window_result_controller.result.fantastic;
		}

		return output;
	}

	public void load_level(int input_level){

		is_dan = false;
		current_level = input_level;

		undo_count = 0;


		initialize_data ();
		loader.load_data (current_level+1);
	}

	public void load_dan(int input_dan){

		is_dan = true;
		current_dan_stage = 0;
		current_dan_ints = pick_random_ints (range_to_ints (settings.dan_range_min[input_dan], settings.dan_range_max[input_dan]), settings.max_dan_stage);
		current_dan = input_dan;

		undo_count = 0;
	

		initialize_data ();

		is_reflected_x = (Random.Range (0, 2) == 0 ? true : false);
		is_reflected_y = (Random.Range (0, 2) == 0 ? true : false);

		rotation_angle = 90 * Random.Range (0, 4);

		loader.load_data (current_dan_ints[current_dan_stage]);
	}

	public void load_dan_again(){
		current_dan_stage++;

	
		initialize_data ();

		is_reflected_x = (Random.Range (0, 2) == 0 ? true : false);
		is_reflected_y = (Random.Range (0, 2) == 0 ? true : false);

		rotation_angle = 90 * Random.Range (0, 4);

		loader.load_data (current_dan_ints[current_dan_stage]);
	}

	public void initialize_data(){

		is_hint_mode = false;
		is_answer_mode = false;
		is_cleared = false;

		is_reflected_x=false;
		is_reflected_y=false;
		rotation_angle=0;

		selected_lines=new hitofude_line_controller[0];
		selected_points=new hitofude_point_controller[0];

		//ui.UI_touch_panel_position.localPosition = new Vector3 (0, settings.get_hitofude_offset_y(),0);
	}

	public bool is_point_available(hitofude_point_controller input_point){
		bool output = false;

		if (is_answer_mode) {

			if (answer [selected_points.Length] == input_point.id) {
				output = true;
			}


		}else if (is_hint_mode&&selected_points.Length==0) {

				if(answer[0]==input_point.id){
					output=true;
				}
		}else{

			if(selected_points.Length==0){

				output=true;

			}else{

				hitofude_line_controller temp_line;

				hitofude_point_controller.line_availability temp=selected_points[selected_points.Length-1].check_line_availabiliy(input_point.id,selected_points.Length,out temp_line);

				if(temp==hitofude_point_controller.line_availability.yes){
					output=true;


				}

				if(temp==hitofude_point_controller.line_availability.reverse||temp==hitofude_point_controller.line_availability.reverse_and_quest){
					if(settings.blink_arrow_when_wrong_direction){
						if(temp_line!=null){
							temp_line.arrow_animation_state=hitofude_line_controller.Arrow_animation_state.start;
						}
					}
				}

				if(temp==hitofude_point_controller.line_availability.quest||temp==hitofude_point_controller.line_availability.reverse_and_quest){
					if(settings.blink_label_when_wrong_turn){
						if(temp_line!=null){
							temp_line.label_animation_state=hitofude_line_controller.Label_animation_state.start;
						}
					}

				}
			
			}
		}

		return output;

	}

	public void show_dragged_line(Vector2 input_position){

		if(selected_points.Length!=0&&selected_lines.Length!=lines.Length){
			dragged_line.parent.SetActive (true);

			dragged_line.draw_line (selected_points[selected_points.Length-1].transform.localPosition, input_position);
		}
		//hitofude_draw_controller.draw_line (dragged_line.gameObject);
	}

	public void hide_dragged_line(){
		dragged_line.parent.SetActive (false);
	}

	public void button_answer_yes_pressed(){
		//ui.answer_point.position.localPosition = find_point_by_id (answer[0]).transform.localPosition;

		ui.answer_point.set_position (find_point_by_id (answer[0]).transform.localPosition);
		ui.answer_point.animation_start ();

		button_restart_pressed (false);
		is_answer_mode = true;
		is_hint_mode = false;

		hide_dragged_line();
	}

	public void button_hint_yes_pressed(){

		if(!is_answer_mode){


			//ui.answer_point.position.localPosition = find_point_by_id (answer[0]).transform.localPosition;

			ui.answer_point.set_position (find_point_by_id (answer[0]).transform.localPosition);
			ui.answer_point.animation_start();



			button_restart_pressed (false);
			is_hint_mode = true;

			hide_dragged_line();

		}
	}

	public void disable_hint_mode(){
		is_hint_mode = false;
	}

	public int[] range_to_ints(int input_min, int input_max){

		int[] output = new int[0];
		if(input_max>=input_min){
			output = new int[input_max - input_min + 1];

			int count=0;
			for(int i= input_min ;i<=input_max; i++){
				output[count]=i;
				count++;
			}
		}

		return output;

	}

	public int[] pick_random_ints (int[] input_ints,int number_of_pick){
		int[] output=  new int[0];
		int[] remaining = input_ints;

		if(input_ints.Length!=0){

			for (int i=1;i<=number_of_pick;i++){

				int picked_int=input_ints[0];
				int picked_slot=0;

				if(remaining.Length!=0){

					picked_slot=Random.Range(0,remaining.Length);
					//print (picked_slot);
					picked_int=remaining[picked_slot];
					remaining=delete_int(remaining,picked_slot);
				}

				output=add_int(output,picked_int);
			}

		}else{

		}

		return output;

	}

	public int[] add_int(int[] input_ints, int added_int){
		int[] output = new int[input_ints.Length+1];

		for (int i=0; i<input_ints.Length; i++) {
			output[i]=input_ints[i];
		}

		output [input_ints.Length] = added_int;

		return output;

	}

	public int[] delete_int(int[] input_ints, int deleted_slot){
		int[] output = new int[0];
		
		for (int i=0; i<input_ints.Length; i++) {
			if(i!=deleted_slot){
				output=add_int(output,input_ints[i]);
			}
		}
		
		return output;
		
	}

	public void refresh_point_availability(){

		ui.UI_label_turn.GetComponent<UILabel> ().text = "Turn " + (selected_lines.Length+1);

		if(selected_points.Length>0){

			for(int i=0; i<selected_points.Length-1;i++){
				
				selected_points[i].enable_touch();
			}

			selected_points[selected_points.Length-1].disable_touch();
		}



		if(settings.make_line_gray_when_quest){

			if (quest [selected_lines.Length] ) {


				foreach (hitofude_line_controller one_line in lines) {

					if(answer_lines[selected_lines.Length]!=one_line){

						one_line.quest_mode_gray ();
					}else{
						one_line.quest_mode_ungray ();
					}
					
				}

				foreach (hitofude_point_controller one_point in points) {

					hitofude_line_controller temp =answer_lines[selected_lines.Length];

					if(!temp.has_point(one_point)){

						one_point.quest_mode_gray ();
					}else{
						one_point.quest_mode_ungray ();
					}
				}


			} else {

				foreach(hitofude_line_controller one_line in lines){
				
					one_line.quest_mode_ungray ();
					
				}

				foreach (hitofude_point_controller one_point in points){
					one_point.quest_mode_ungray();
				}
			}
		}
	}

	public void rescale_lines_and_points(float scale_factor){

		foreach (hitofude_point_controller temp in points) {
			temp.rescale_point(scale_factor);
		}

		foreach (hitofude_line_controller temp in lines) {
			temp.rescale_arrow_and_label(scale_factor);
		}

		dragged_line.rescale_arrow_and_label (scale_factor);
		ui.answer_point.scale.localScale = new Vector3 (scale_factor,scale_factor,1);
	}





}
