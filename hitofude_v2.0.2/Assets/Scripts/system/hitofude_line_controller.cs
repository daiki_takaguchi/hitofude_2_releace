﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class hitofude_line_controller : MonoBehaviour {

	// Use this for initialization

	public GameObject parent; 


	public hitofude_data_controller hitofude_data;

	public hitofude_point_controller start_point;

	public hitofude_point_controller end_point;

	public BoxCollider box;

	public Transform scale_arrow_1st;
	public Transform scale_arrow_2nd;
	public Transform scale_label;

	public Transform position_arrow_parent;
	public Transform position_arrow_1st;
	public Transform position_arrow_2nd;
	public Transform position_label;

	public UIWidget line_texture;
	public UIWidget arrow_1st_texture;
	public UIWidget arrow_2nd_texture;
	public UIWidget label_back_texture;
	public UILabel label;
	
	//public List<int> answer_no_list = new List<int> ();

	public int max_selection_count;
	public int current_selection_count;
	public int answer_set_count=0;
	public int quest_set_count=0;

	public List<hitofude_line_data> line_data= new List<hitofude_line_data>();
	
	public bool is_gray=false;




	public enum Arrow_animation_state{
		none,
		start,
		base_color,
		red,
		end

	}

	public enum Label_animation_state{
		none,
		start,
		base_color,
		red,
		end
	}

	public Arrow_animation_state arrow_animation_state;

	public float prev_arrow_blink_time;

	public int current_arrow_blinc_times;
	
	public Label_animation_state label_animation_state;

	public float prev_label_blink_time;

	public int current_label_blinc_times;

	//private Color normal_color = new Color32 (15, 103, 144,255);

	void Start () {

		float temp = hitofude_data.settings.line_collider_width/hitofude_data.settings.line_width;

		box.size = new Vector3 (1,temp,0.5f);

		box.enabled = hitofude_data.settings.is_line_collider_available;
		
	}
	
	// Update is called once per frame
	void Update () {

		if (arrow_animation_state == Arrow_animation_state.start) {

			initialize_arrow_animation();


		}else if(arrow_animation_state == Arrow_animation_state.base_color){



			
			if(Time.time>(prev_arrow_blink_time+hitofude_data.settings.blink_arrow_interval)){

				print ("base");
				current_arrow_blinc_times++;

				if(current_arrow_blinc_times>=hitofude_data.settings.blink_arrow_times){
					end_arrow_animation();
				}else{


					prev_arrow_blink_time=Time.time;

					arrow_1st_texture.color= hitofude_data.settings.base_color;
					arrow_2nd_texture.color= hitofude_data.settings.base_color;

					arrow_animation_state=Arrow_animation_state.red;
				}

			}


		}else if(arrow_animation_state==Arrow_animation_state.red){



			if(Time.time>(prev_arrow_blink_time+hitofude_data.settings.blink_arrow_interval)){

				print ("red");
				current_arrow_blinc_times++;
				
				if(current_arrow_blinc_times>=hitofude_data.settings.blink_arrow_times){


					end_arrow_animation();

				}else{
					
					
					prev_arrow_blink_time=Time.time;
					
					arrow_1st_texture.color= hitofude_data.settings.red;
					arrow_2nd_texture.color= hitofude_data.settings.red;
					
					arrow_animation_state=Arrow_animation_state.base_color;
				}
			}



		}


		if (label_animation_state == Label_animation_state.start) {
			
			initialize_label_animation();
			
			
		}else if(label_animation_state == Label_animation_state.base_color){
			
			
			
			
			if(Time.time>(prev_label_blink_time+hitofude_data.settings.blink_label_interval)){
				
				//print ("base");
				current_label_blinc_times++;
				
				if(current_label_blinc_times>=hitofude_data.settings.blink_label_times){
					end_label_animation();
				}else{
					
					
					prev_label_blink_time=Time.time;
					
					label_animation_state=Label_animation_state.red;

					label.color = hitofude_data.settings.base_color;
				}
				
			}
			
			
		}else if(label_animation_state==Label_animation_state.red){
			
			
			
			if(Time.time>(prev_label_blink_time+hitofude_data.settings.blink_label_interval)){
				
				//print ("base");
				current_label_blinc_times++;
				
				if(current_label_blinc_times>=hitofude_data.settings.blink_label_times){
					end_label_animation();
				}else{
					
					
					prev_label_blink_time=Time.time;
					
					label_animation_state=Label_animation_state.base_color;
					
					label.color = hitofude_data.settings.red;
				}
				
			}
			
			
			
		}


	
	}

	public void initialize_arrow_animation(){

		prev_arrow_blink_time=Time.time;
		arrow_animation_state=Arrow_animation_state.red;
		current_arrow_blinc_times=0;

	}

	public void end_arrow_animation(){

		arrow_animation_state=Arrow_animation_state.none;
		current_arrow_blinc_times=0;
		prev_arrow_blink_time = 0;

		arrow_1st_texture.color= hitofude_data.settings.base_color;
		arrow_2nd_texture.color= hitofude_data.settings.base_color;


	}

	public void initialize_label_animation(){
		
		//print ("animation_started");
		
		prev_label_blink_time=Time.time;
		label_animation_state=Label_animation_state.red;
		current_label_blinc_times=0;
		
	}
	
	public void end_label_animation(){
		
		label_animation_state = Label_animation_state.none;
		current_label_blinc_times = 0;
		prev_label_blink_time = 0;
		
		label.color = hitofude_data.settings.base_color;
	}

	public void initialize(hitofude_data_controller input_hitofude_data,hitofude_point_controller input_start_point,hitofude_point_controller input_end_point, bool input_is_one_direction,int  input_max_selection_count){

		start_point = input_start_point;
		start_point.add_connected_line (this);
		end_point = input_end_point;
		end_point.add_connected_line (this);




		max_selection_count = input_max_selection_count;

		for (int i=0;i<input_max_selection_count;i++){

			hitofude_line_data temp=new hitofude_line_data();
			temp.is_one_direction= input_is_one_direction;
			line_data.Add (temp);

		}

		if(!input_is_one_direction){
			position_arrow_1st.gameObject.SetActive(false);
			position_arrow_2nd.gameObject.SetActive(false);
		}else{
			position_arrow_1st.gameObject.SetActive(true);
			position_arrow_2nd.gameObject.SetActive(true);
		}

	
		set_width (input_hitofude_data);

		draw_line ( new Vector3 (start_point.transform.localPosition.x, start_point.transform.localPosition.y, 0),new  Vector3 (end_point.transform.localPosition.x, end_point.transform.localPosition.y, 0));

		string arrow_string = "<->";

		if(input_is_one_direction){
			arrow_string ="->";
		}

		parent.name = "line_parent "+input_start_point.id+arrow_string+input_end_point.id;

		//refresh_line ();
	}

	public void undo(){
		current_selection_count--;
	
		refresh_line ();
	}

	public void refresh_line(){

		end_arrow_animation();

		int remaining_count = max_selection_count - current_selection_count;

		if (remaining_count <= 0) {
		
			change_color(hitofude_data.settings.red);



		} else if (remaining_count == 1) {

			change_color(hitofude_data.settings.base_color);


		} else {

			change_color(hitofude_data.settings.blue);

		}

		label_back_texture.color = hitofude_data.settings.white;

		//int current_selection_count_temp = current_selection_count;

		if(current_selection_count<max_selection_count){

			position_label.gameObject.SetActive(line_data[current_selection_count].is_quest);

			if(line_data[current_selection_count].is_quest){

				int answer_no= line_data[current_selection_count].answer_no;

				label.text = ((answer_no ) % 100)+"";
				
				int char_count=1;
				if(answer_no>9){
					char_count=2;
				}
				
				float char_width = 0.6f*(char_count);
				
				label_back_texture.transform.localScale =  new Vector3(hitofude_data.settings.line_number_pixel*char_width,hitofude_data.settings.line_number_pixel,1);
				label.transform.localScale =  new Vector3(hitofude_data.settings.line_number_pixel,hitofude_data.settings.line_number_pixel,1);

			}
		}
	
	}

	public void change_color(Color input_color){

		line_texture.color = input_color;
		arrow_1st_texture.color = input_color;
		arrow_2nd_texture.color = input_color;
		label.color = input_color;
		
	}

	public void set_answer(int input_int){

		line_data [answer_set_count].answer_no = input_int;


		parent.name += " ans:" + input_int;

		answer_set_count++;
	}

	public void set_answer_text(){
		/*

		label.text = ((input_int ) % 100)+"";
		
		int char_count=1;
		if(input_int>9){
			char_count=2;
		}
		
		float char_width = 0.6f*(char_count);
		
		label_back_texture.transform.localScale =  new Vector3(hitofude_data.settings.line_number_pixel*char_width,hitofude_data.settings.line_number_pixel,1);
		label.transform.localScale =  new Vector3(hitofude_data.settings.line_number_pixel,hitofude_data.settings.line_number_pixel,1);
		*/
	
	}

	public void set_quest(bool input_quest){

		line_data [quest_set_count].is_quest = input_quest;

		if (input_quest) {
		
			parent.name += " q";


		}

	



		quest_set_count++;


	}
	/*
	public void set_one_direction(bool input_is_one_direction,bool input_is_reverse){

		//is_one_direction_list.Add(input_is_one_direction);

		//is_reverse_list.Add (input_is_reverse);

		
		if(!input_is_one_direction){
			
			position_arrow_1st.gameObject.SetActive(false);
			position_arrow_2nd.gameObject.SetActive(false);
		}else{
			position_arrow_1st.gameObject.SetActive(true);
			position_arrow_2nd.gameObject.SetActive(true);
			//arrow_2nd_texture.transform.parent.gameObject.SetActive(true);
		}

	
	}*/



	public void set_width(hitofude_data_controller input_hitofude_data){

		hitofude_data = input_hitofude_data;

		Vector3 temp = transform.localScale;
		transform.localScale = new Vector3 (temp.x,input_hitofude_data.settings.line_width,temp.z);

		arrow_1st_texture.transform.localScale=new Vector3 (input_hitofude_data.settings.arrow_width,input_hitofude_data.settings.arrow_height,1);
		arrow_2nd_texture.transform.localScale=new Vector3 (input_hitofude_data.settings.arrow_width,input_hitofude_data.settings.arrow_height,1);


	}

	public void draw_line(Vector3 from_2d,Vector3 to_2d){

		Vector3 from_2d_no_z = new Vector3 (from_2d.x, from_2d.y, 0);
		Vector3 to_2d_no_z = new Vector3 (to_2d.x, to_2d.y, 0);

		float length = (to_2d - from_2d).magnitude;
		
	
		

		Vector3 temp = transform.localPosition;
		transform.localPosition =  new Vector3(from_2d.x,from_2d.y,temp.z);

		position_arrow_parent.localPosition = from_2d_no_z;
		position_label.localPosition =  from_2d_no_z;

		temp = transform.localScale;
		transform.localScale = new Vector3 (length*2,temp.y,temp.z);


		position_arrow_1st.localPosition =  new Vector3(length/4.0f,0,0);

		position_arrow_2nd.localPosition =  new Vector3(length*3/4.0f,0,0);

		position_label.localPosition =  new Vector3(length/2.0f,0,0);


		Vector3 rotation_z = Quaternion.FromToRotation (new Vector3 (1, 0, 0), (to_2d_no_z - from_2d_no_z).normalized).eulerAngles;

		if((to_2d_no_z - from_2d_no_z).normalized==new Vector3 (-1, 0, 0)){
			rotation_z =  new Vector3(0,0,180);
		}

		Quaternion rotation = Quaternion.Euler (new Vector3(0,0,rotation_z.z));
		Quaternion reverse_rotation =Quaternion.Euler (new Vector3(0,0,-rotation_z.z));

		transform.localRotation=rotation;

		position_arrow_parent.localRotation = rotation;


		//position_label.localRotation = rotation;

		scale_label.localRotation =reverse_rotation;


	
	


		//arrow_2nd_texture.transform.parent.localRotation=Quaternion.FromToRotation (new Vector3(1,0,0),(to_2d-from_2d).normalized);

		
	} 

	public void selected(){

		current_selection_count++;
		refresh_line ();

	}


	/*
	public void change_state(line_state input_state){
		state=input_state;

		end_arrow_animation();

		if(input_state==line_state.selected){
			line_texture.color= hitofude_data.settings.red;
			arrow_1st_texture.color= hitofude_data.settings.red;
			arrow_2nd_texture.color= hitofude_data.settings.red;

		}else if(input_state==line_state.normal){
			line_texture.color= hitofude_data.settings.base_color;
			arrow_1st_texture.color= hitofude_data.settings.base_color;
			arrow_2nd_texture.color= hitofude_data.settings.base_color;
		}

		label_back_texture.color = hitofude_data.settings.white;
		label.color = hitofude_data.settings.base_color;
		 
	}*/

	public void quest_mode_gray(){


		if(current_selection_count<max_selection_count){
			is_gray=true;

			quest_mode_gray_if_gray();

		}
	

	}

	public void quest_mode_gray_if_gray(){

		if(is_gray){

			end_arrow_animation();

			line_texture.color = hitofude_data.settings.point_unavailable;
			arrow_1st_texture.color = hitofude_data.settings.point_unavailable;
			arrow_2nd_texture.color = hitofude_data.settings.point_unavailable;
		}
	}

	public void quest_mode_ungray(){

		if (is_gray) {

			is_gray=false;
			refresh_line();
		}
	}



	public void rescale_arrow_and_label(float scale_factor){
		scale_arrow_1st.localScale = new Vector3 (scale_factor,scale_factor,1);
		scale_arrow_2nd.localScale = new Vector3 (scale_factor,scale_factor,1);
		scale_label.localScale = new Vector3 (scale_factor,scale_factor,1);

		Vector3 temp = transform.localScale;

		transform.localScale= new Vector3 (temp.x,hitofude_data.settings.line_width*scale_factor,temp.z);
	}

	public hitofude_point_controller.line_availability check_line_availability(int input_answer_no,bool input_is_quest,int input_start_point_id,int input_end_point_id){

		hitofude_point_controller.line_availability output = hitofude_point_controller.line_availability.yes;

		if (current_selection_count >= max_selection_count) {

			output = hitofude_point_controller.line_availability.no;

		} else {


			if (is_current_one_direction ()) {

				if (is_reverse (input_start_point_id, input_end_point_id)) {
					output = hitofude_point_controller.line_availability.reverse;
				}

			}


			if (input_is_quest) {
				if (!is_current_answer_no_correct (input_answer_no)) {
					if (output == hitofude_point_controller.line_availability.reverse) {
						output = hitofude_point_controller.line_availability.reverse_and_quest;
					} else {
						output = hitofude_point_controller.line_availability.quest;
					}
				

				} else {


				}
			}else{
				if(is_current_quest()){

					if (output == hitofude_point_controller.line_availability.reverse) {
						output = hitofude_point_controller.line_availability.reverse_and_quest;
					} else {
						output = hitofude_point_controller.line_availability.quest;
					}

				}
			}

		}


		//print (output);

		return output;

	}



	public bool is_this_line(int input_start_point_id,int input_end_point_id){



		bool output = false;


		if(((input_start_point_id==start_point.id)&&(input_end_point_id==end_point.id))||
		   ((input_end_point_id==start_point.id)&&(input_start_point_id==end_point.id))){


			output=true;
		
		

		}


		return output;


	}

	public bool is_current_answer_no_correct(int input_answer_no){
		return line_data [current_selection_count].answer_no == input_answer_no;
	}


	public bool is_current_quest(){

		return line_data[current_selection_count].is_quest;

	}

	public bool is_current_one_direction(){
		return line_data[current_selection_count].is_one_direction;
		
	}



	public bool is_reverse(int input_start_point_id, int input_end_point_id){

		bool output = line_data[current_selection_count].is_reverse;

		if((input_end_point_id==start_point.id)&&(input_start_point_id==end_point.id)){

			output=!output;
			
		}
		
		return output;
		
	}

	public bool has_point(hitofude_point_controller input_point){

		bool output = false;

		if((input_point==start_point)||(input_point==end_point)){
			output=true;
		}

		return output;
	
	
	}






	void OnPress(bool is_pressed) {
		
		if(is_pressed){
			hitofude_data.ui.UI_button_touch_panel.GetComponent<button_touch_panel_controller> ().non_drag_mode = true;
		}
		//print ("pressed:"+is_pressed);
		//hitofude_data.ui.button_point_pressed (this);
	}

}
