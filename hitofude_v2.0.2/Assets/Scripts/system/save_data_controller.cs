﻿using UnityEngine;
using System;
using System.Collections;

public class save_data_controller : MonoBehaviour {

	// Use this for initialization

	public hitofude_data_controller hitofude_data;

	void Start () {

		//PlayerPrefs.DeleteAll ();

		if(hitofude_data.settings.clear_data){
			PlayerPrefs.DeleteAll ();
		}

		/*
		if(!PlayerPrefs.HasKey("initialized")){
			initialize();
		}
		*/

		if(!PlayerPrefs.HasKey("level_0")){
			PlayerPrefs.SetInt("level_0",1);
		}

		if(!PlayerPrefs.HasKey("dan_0")){
			PlayerPrefs.SetInt("dan_0",1);
		}


	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void initialize(){

		//PlayerPrefs.SetInt ("initialized",1);
		PlayerPrefs.SetInt("level_0",1);
		PlayerPrefs.SetInt("dan_0",1);
		PlayerPrefs.Save ();

	}


	public button_select_level_controller.level_state get_select_level_data(int level_no){
		button_select_level_controller.level_state output = button_select_level_controller.level_state.Locked;

		string label = "level_" + level_no;

		if(PlayerPrefs.HasKey(label)){
			output=int_to_level_state(PlayerPrefs.GetInt(label));
		}
		

		return output;
	}

	public button_select_level_controller.level_state get_select_dan_data(int dan_no){
		button_select_level_controller.level_state output = button_select_level_controller.level_state.Locked;
		
		string label = "dan_" + dan_no;

		if(PlayerPrefs.HasKey(label)){

			output=int_to_level_state(PlayerPrefs.GetInt(label));
	
		}
		
		
		return output;
	}



	public void set_select_level_data(int level_no,button_select_level_controller.level_state input_state){
		int temp = level_state_to_int (input_state);

		string label = "level_" + level_no;

		if(PlayerPrefs.HasKey(label)){
			if(PlayerPrefs.GetInt(label)<temp){
				PlayerPrefs.SetInt(label,temp);
				PlayerPrefs.Save();
			}
		}else{
			PlayerPrefs.SetInt(label,temp);
			PlayerPrefs.Save();
		}
	}

	public void set_select_dan_data(int level_no,button_select_level_controller.level_state input_state){
		int temp = level_state_to_int (input_state);
		
		string label = "dan_" + level_no;
		
		if(PlayerPrefs.HasKey(label)){
			if(PlayerPrefs.GetInt(label)<temp){
				PlayerPrefs.SetInt(label,temp);
				PlayerPrefs.Save();
			}
		}else{
			PlayerPrefs.SetInt(label,temp);
			PlayerPrefs.Save();
		}
	}

	public int level_state_to_int (button_select_level_controller.level_state input_state){
		int output = 0;
		
		if(input_state==button_select_level_controller.level_state.Locked){
			output=0;
		}else if(input_state==button_select_level_controller.level_state.Zero){
			output=1;
		}else if(input_state==button_select_level_controller.level_state.One){
			output=2;
		}else if(input_state==button_select_level_controller.level_state.Two){
			output=3;
		}else if(input_state==button_select_level_controller.level_state.Three){
			output=4;
		}
		
		return output;
	}
	

	public button_select_level_controller.level_state int_to_level_state(int input_int){
		button_select_level_controller.level_state output = button_select_level_controller.level_state.Locked;
		
		if(input_int==0){
			output=button_select_level_controller.level_state.Locked;
		}else if(input_int==1){
			output=button_select_level_controller.level_state.Zero;
		}else if(input_int==2){
			output=button_select_level_controller.level_state.One;
		}else if(input_int==3){
			output=button_select_level_controller.level_state.Two;
		}else if(input_int==4){
			output=button_select_level_controller.level_state.Three;
		}
		
		
		return output;
	}

	public string get_level_progress(){

		int total_score = 0;
		int max = hitofude_data.settings.max_level;


		//if(is_dan){
		//	max=hitofude_data.settings.max_dan;
		//}


		for (int i =0; i < max;i++){

			button_select_level_controller.level_state temp =button_select_level_controller.level_state.Locked;

			//if(is_dan){
			//	temp =get_select_dan_data(i);
			//}else{
				temp =get_select_level_data(i);
			//}

			int score=0;

			if(temp==button_select_level_controller.level_state.Locked){
				score=0;
			}else if(temp==button_select_level_controller.level_state.Zero){
				score=0;
			}else if(temp==button_select_level_controller.level_state.One){
				score=1;
			}else if(temp==button_select_level_controller.level_state.Two){
				score=2;
			}else if(temp==button_select_level_controller.level_state.Three){
				score=3;
			}


			total_score+=score;
		}

		string output = "";

		if(total_score==max*3){
			output = "★100%";
		
		
		}else {

			float total_score_float= Mathf.RoundToInt (1000*total_score/(max*3.0f))/10.0f;


			if(total_score_float<=0.1f&&total_score>0){
				total_score_float=0.1f;
			}

			if(total_score_float>=100.0f){
				total_score_float=99.9f;
			}

			output = "★"+total_score_float.ToString("00.0")+"%";
		}

		//print (total_score);

		return output;


	}


	public string get_dan_progress(){
		//string label;

		int cleared = -1;
		int max = hitofude_data.settings.max_dan;

		for (int i =0; i < max; i++) {
			string label="dan_"+i;

			if(PlayerPrefs.HasKey(label)){
				if(PlayerPrefs.GetInt(label)>1){
					cleared=i;
				}

			}
		}

		string output="";

		if(cleared>-1){
			output=hitofude_data.settings.dan_list[cleared];

		}

		return output;

		
	}
	

			



	public void hint_used(){
		PlayerPrefs.SetString ("hint", get_unix_time_now().ToString());
		PlayerPrefs.Save ();
	}

	public void answer_used(){
		PlayerPrefs.SetString ("answer", get_unix_time_now().ToString ());
		PlayerPrefs.Save ();
	}

	public long remaining_time_hint(){

		//long output = 7200;

		long output = hitofude_data.settings.hint_interval_in_seconds;
		//print (PlayerPrefs.GetString("hint"));

		if(PlayerPrefs.HasKey("hint")){
			output-=(get_unix_time_now()-long.Parse(PlayerPrefs.GetString("hint")));
		}else{
			output=0;
		}



		if(output<0){
			output=0;
		}



		return output;


		
	}

	public long remaining_time_answer(){
		//long output = 86400;
		//output = 5;
		long output = hitofude_data.settings.answer_interval_in_seconds;
		
		if(PlayerPrefs.HasKey("answer")){
			
			output-=(get_unix_time_now()-long.Parse(PlayerPrefs.GetString("answer")));
		}else{
			output=0;
		}
		
		
		if(output<=0){
			output=0;
			PlayerPrefs.DeleteKey ("answer");
		}
		
		return output;
		
	}
	/*
	public long get_time_from_tutorial(){

		DateTime UnixEpoch =new DateTime (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		//return (long) (DateTime.UtcNow - UnixEpoch).TotalSeconds;

		long current_time= get_unix_time_now();

		long output= current_time- (long) (UnixEpoch - UnixEpoch).TotalSeconds;

		if(PlayerPrefs.HasKey("tutorial_time")){
			output= current_time- long.Parse(PlayerPrefs.GetString("tutorial_time"));
		}

		PlayerPrefs.SetString ("tutorial_time", get_unix_time_now().ToString());

		return output;
	
	}*/

	public bool has_tutorial_shown(int level,bool is_dan){

		string key = "tutorial_" + level +"_"+ is_dan;


		bool output = false;

		if(PlayerPrefs.HasKey(key)){
			output=true;
		}

		PlayerPrefs.SetInt (key,0);

		return output;
	
	}

	public bool is_dan_ready(){
		bool output = false;

		if(PlayerPrefs.HasKey("level_"+hitofude_data.settings.dan_unlock_level)){
			output=true;
		}

		if(PlayerPrefs.HasKey ("is_dan_ready")){
			output=true;
		}


		return output;
	
	}

	public void set_dan_ready(){
		PlayerPrefs.SetInt ("is_dan_ready",0);
	}

	public long get_unix_time_now(){
		DateTime UnixEpoch =new DateTime (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		return (long) (DateTime.UtcNow - UnixEpoch).TotalSeconds;
	}


}
