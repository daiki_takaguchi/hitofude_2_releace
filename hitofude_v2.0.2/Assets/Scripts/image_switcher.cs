﻿using UnityEngine;
using System.Collections;

public class image_switcher : MonoBehaviour {

	public GameObject japanese;
	public GameObject english;


	void Start () {

		if (GameObject.FindObjectOfType<ad_controller> ().is_japanese_function()) {
			japanese.SetActive (true);
		
			english.SetActive(false);
		} else {

			japanese.SetActive (false);
			
			english.SetActive(true);
		
		}
		
	}



}
