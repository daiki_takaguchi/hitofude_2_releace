import processing.core.*; 
import processing.xml.*; 

import java.applet.*; 
import java.awt.Dimension; 
import java.awt.Frame; 
import java.awt.event.MouseEvent; 
import java.awt.event.KeyEvent; 
import java.awt.event.FocusEvent; 
import java.awt.Image; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class hitohude extends PApplet {

int x,y;
int fileid=0;

String filenames="no files";

int [][]h_point=new int[64][4]; ///\u5206\u5c90\u70b9\u8a2d\u5b9a 0:use1 1:X\u5ea7\u6a19 2:Y\u5ea7\u6a19 3:tmp
int [][]h_line=new int[128][4];  ///\u7dda\u8a2d\u5b9a 0:use 1:\u958b\u59cbh_point 2:\u7d42\u7aefh_point 3:type

int []h_clear_course=new int[256]; ///\u30af\u30ea\u30a2\u30eb\u30fc\u30c8
int []h_clear_quest_course=new int[256]; ///\u30af\u30ea\u30a2\u30eb\u30fc\u30c8
int []h_play=new int[256]; ///PLAY\u30eb\u30fc\u30c8
int step=0;
int message=0;
int messagetime=120;
int edittype=0;
int glid=0;
int glid_lock=0;
int cpoint=-1;
int anscount=-1;
int anserdisp=0;

int playpoint=-1;
int []h_line_ck=new int[256];
int playclear=0;
int tern=0;
int counter=0;

public void setup()
{
  size(1000, 1000);
  colorMode(RGB, 100);
  background(100);
  rectMode(CENTER);
  frameRate(30);
  x = 0;
  y = 0;
  for(int i=0;i<64;i++){
    h_point[i][0]=0; 
    h_point[i][1]=0; 
    h_point[i][2]=0;
    h_point[i][3]=0; 
    
 
  }
   for(int i=0;i<128;i++){
     h_line[i][0]=0;
   }
   for(int i=0;i<256;i++){
     h_line_ck[i]=0;
     h_clear_course[i]=-1;
     h_clear_quest_course[i]=0;
     h_play[i]=0;
   }
}

public void draw()
{
  int i;
 
  background(100);	//\u80cc\u666f\u3092\u5857\u308a\u3064\u3076\u3059\u5834\u5408\u306f\u3053\u3063\u3061\u3092\u4f7f\u3046
 
  if(glid>=1){
    for(i=0;i<40;i++){
      strokeWeight(1);
       if(i*25*glid%500==0){
          strokeWeight(2); 
       }
      if(i*25*glid%100==0){
        stroke(40);
        
      }else{
        stroke(80);
      }
      line(0, i*25*glid,1024, i*25*glid); 
      line(i*25*glid,0,i*25*glid,1024); 
    } 
   
  }
 
  fill(0);		//\u6587\u5b57\u306e\u8272
  textSize(24);		//\u6587\u5b57\u306e\u30b5\u30a4\u30ba(\u30d4\u30af\u30bb\u30eb\u6570)
  //text("FILEID:"+nf(fileid,4),32, 32); //\u8868\u793a\u3059\u308b\u6587\u5b57\u3068\u4f4d\u7f6e
  if(step==0){
    text("EDITMODE",32,96);
    
    for(i=1;i<10;i++){
      if(i==edittype){
        fill(128,0,0); 
      }else{
        fill(0,0,0);   
      }
      switch(i){
        
      case 1:
        text((i)+":POINTSET",(i-1)*160+32,128);
        break;
      case 2:
        text((i)+":LINESET",(i-1)*160+32,128);
        break;
       case 3:
        text((i)+":LINE2SET",(i-1)*160+32,128);
        break;  
       case 4:
        text((i)+":DELPOINT",(i-1)*160+32,128);
        break;  
       case 5:
        text((i)+":DELLINE",(i-1)*160+32,128);
        break;  
       case 6:
        text((i)+":POINTMOVE",(i-1)*160+32,128);
        break;
       case 7:
        text((i)+":ANSERSET",(i-7)*160+32,160);
        break;
       case 8:
        text((i)+":ANStoQUEST",(i-7)*160+32,160);
        break;
       case 9:
        text((i)+":DOUBLE",(i-7)*160+32,160);
        break;
      }
    }
 //   text("1:POINTSET 2:LINESET 3:LINE2SET 4:DELPOINT 5:DELLINE",32, 128); //\u8868\u793a\u3059\u308b\u6587\u5b57\u3068\u4f4d\u7f6e 
  }
  if(step==1){
    text("GAMEPLAY",32,96);
  
    text("1:RESTART  B:BACK",32,128);
    
  }
  fill(0);
  text("E:EDIT   P:GAMEPLAY   L:load    S:Save   G:GLID   R:GLOCK  A:ANSER",32, 64); //\u8868\u793a\u3059\u308b\u6587\u5b57\u3068\u4f4d\u7f6e
  text("filename:"+filenames,32,32);
    
  if(messagetime>0){
    int messy=160+320;
    if(message==1){
      text("SAVE OK!!",32,messy);  
    } 
    if(message==2){
      text("LOAD OK!!",32,messy);  
    }   
    if(message==3){
      text("LOAD ERROR!!",32,messy);  
    }
    if(message==4){
      text("LOAD ERROR!! NOT GAMEDATA",32,messy);  
    }
    if(message==5){
      text("LOAD ERROR!! VERSION",32,messy);  
    }
    if(message==6){
      text("LOAD ERROR!! BREAK",32,messy);  
    }  
    if(message==10){
      text("POINT OVER  max64!!",32,messy);  
    }
   if(message==11){
      text("LINE OVER  max128!!",32,messy);  
    }    
    messagetime--; 
  }
  
  fill(0,128,128);
  if(glid_lock==1 && glid>0){
    float rx=(mouseX+(glid*25)/2)/(glid*25)*(glid*25);
    float ry=(mouseY+(glid*25)/2)/(glid*25)*(glid*25);
    rect(rx, ry, 10, 10);	//(X,Y)\u306e\u4f4d\u7f6e\u306b\u56db\u89d2\u3092\u63cf\u304f
  }
  fill(0);
  for(i=0;i<128;i++){
    if(h_line[i][0]==1){ 
      if(h_point[h_line[i][1]][0]==1 && h_point[h_line[i][2]][0]==1 &&  (h_line[i][3]==0 || h_line[i][3]==2)){
        int dx1=h_point[h_line[i][1]][1];
        int dy1=h_point[h_line[i][1]][2];
        int dx2=h_point[h_line[i][2]][1];
        int dy2=h_point[h_line[i][2]][2];
        
        strokeWeight(10);
        if(step==1 && h_line_ck[i]>0){
          if(h_line[i][3]==0 || (h_line[i][3]==2 && h_line_ck[i]==2)){
            stroke(128,0,0);
          }else{
            stroke(128,60,60);
          }
        }else if(h_line[i][3]==0){
          stroke(0);
        }else if(h_line[i][3]==2){
          stroke(128,0,128);
        }else{
          stroke(0,60,0);
        }
        line(dx1,dy1,dx2,dy2);
      }
      if(h_point[h_line[i][1]][0]==1 && h_point[h_line[i][2]][0]==1 && (h_line[i][3]==1 || h_line[i][3]==3)){
        int dx1=h_point[h_line[i][1]][1];
        int dy1=h_point[h_line[i][1]][2];
        int dx2=h_point[h_line[i][2]][1];
        int dy2=h_point[h_line[i][2]][2];
        for(int vi=0;vi<8;vi++){
          int vdx1=dx1+(dx2-dx1)*vi/8;
          int vdx2=dx1+(dx2-dx1)*(vi+1)/8;
          int vdy1=dy1+(dy2-dy1)*vi/8;
          int vdy2=dy1+(dy2-dy1)*(vi+1)/8;
          strokeWeight(10);
          if(step==1 && ((h_line[i][3]==1 && h_line_ck[i]==1)||(h_line[i][3]==3 && h_line_ck[i]==2))){
           stroke(128,0,0);
          }else if(step==1 && (h_line[i][3]==3 && h_line_ck[i]==1)){
             stroke(10+vi*15,60,60);
          }else if(h_line[i][3]==0){
            stroke(0);
          }else{
            if(h_line[i][3]==3){
              stroke(10+vi*15,0,10+vi*15);
            }else{
              stroke(0,10+vi*15,0);
            }
          }
          
          line(vdx1,vdy1,vdx2,vdy2);
        }
      }
    }
  }
  strokeWeight(1);
  stroke(0);
  fill(0);
  for(i=0;i<64;i++){
    if(h_point[i][0]==1){ 
      int dx=h_point[i][1];   
      int dy=h_point[i][2];  
      if(step==1 && playpoint==i){
         fill(0,0,128); 
      }else if(step==0 && cpoint==i){
        fill(128,0,0); 
      }else{
        fill(0);
      } 	
      ellipse(dx,dy,24, 24); 
    }
  }
  if(anserdisp==1 || (step==0)){
    if(anscount>=0){
      int ax1 = h_point[ h_clear_course[0] ][1];
      int ay1 = h_point[ h_clear_course[0] ][2];
      fill(128,128,0);
      text("S",(ax1)-6,(ay1)+12); 
      
    }
    for(i=1;i<anscount;i++){
      if( h_clear_course[i] < 0) break;
      if( h_clear_quest_course[i-1]==1){
        continue; 
      }
      int ax1 = h_point[ h_clear_course[i-1] ][1];
      int ay1 = h_point[ h_clear_course[i-1] ][2];
      int ax2 = h_point[ h_clear_course[i] ][1];
      int ay2 = h_point[ h_clear_course[i] ][2];
      int dy=0;
      for(int ii=1;ii<i;ii++){
        if(( h_clear_course[i-1]==h_clear_course[ii-1] && h_clear_course[i]==h_clear_course[ii]) || ( h_clear_course[i-1]==h_clear_course[ii] && h_clear_course[i]==h_clear_course[ii-1])){
          dy=24;
        }
      }
      
      fill(128,0,128);
      text((i),(ax1+ax2)/2,(ay1+ay2)/2+dy);
    }
  }
  {
    for(i=1;i<anscount;i++){
      if( h_clear_course[i] < 0) break;
      if( h_clear_quest_course[i-1]==1){
        int ax1 = h_point[ h_clear_course[i-1] ][1];
        int ay1 = h_point[ h_clear_course[i-1] ][2];
        int ax2 = h_point[ h_clear_course[i] ][1];
        int ay2 = h_point[ h_clear_course[i] ][2];
        int dy=0;
        for(int ii=1;ii<i;ii++){
          if(( h_clear_course[i-1]==h_clear_course[ii-1] && h_clear_course[i]==h_clear_course[ii]) || ( h_clear_course[i-1]==h_clear_course[ii] && h_clear_course[i]==h_clear_course[ii-1])){
            dy=24;
          }
        }
        fill(1,128,128);
        if(counter/10%2==1 && (step==1 && tern==i-1)){
          textSize(48);	
        }else{
          textSize(32);
        }
        text((i),(ax1+ax2)/2,(ay1+ay2)/2+dy);
      }
    }
    
  }
  if(step==1){
    if( playclear ==1){
      textSize(48);	
      fill(0,128,128);
      text("CLEAR!!!!!!",300,500);
     
    } 
     textSize(48);	
      fill(0,0,0);
      text("TERN:"+(tern+1),100,200);
  }
  counter=(counter+1)%10000;
}

public int getpoint(int dx,int dy)
{
  int i;
  for( i=0; i<64; i++){
    if(h_point[i][0]==1){ 
      int vx=dx-(h_point[i][1]);
      int vy=dy-(h_point[i][2]);
      if((vx*vx)+(vy*vy)<=24*24){
         return i; 
      }
    }
  }
  return -1;  
}

public void mousePressed() {
  x=mouseX;
  y=mouseY;
  if(step==0){
    if(edittype==1 || (edittype==6 && cpoint>=0)){
      if(glid_lock==1 && glid>0){
        x=(x+(glid*25)/2)/(glid*25)*(glid*25);
        y=(y+(glid*25)/2)/(glid*25)*(glid*25);
      }
    }
  }
  if(step==0){
    if(edittype==1){
      int dx=x;
      int dy=y;
      int ck=0;
      {
        int p=getpoint(dx,dy);
        if(p>=0){
           ck=1;    
        }
      }
      if(ck==0){
        for(int i=0;i<64;i++){
          if(h_point[i][0]==0){
           
            h_point[i][0]=1;
            h_point[i][1]=dx;
            h_point[i][2]=dy;         
            ck=1;
            break; 
          }
        }
      }
      if(ck==0){
        message=10;
        messagetime=120;
      }
    }
    if(edittype==4){
      int dx=x;
      int dy=y;
      int p=getpoint(dx,dy);
      if(p>=0){
        h_point[p][0]=0; 
        for(int i=0;i<128;i++){
          if(h_line[i][0]==1){ 
             if(h_line[i][1]==p || h_line[i][2]==p){
                h_line[i][0]=0;
             } 
          }
        }
      }
    }
    
     if(edittype==6){
      int dx=x;
      int dy=y;
      if(cpoint<0){
        int p=getpoint(dx,dy);
        if(p>=0){
          cpoint=p;
        }
      }else{
        h_point[cpoint][1]=dx;
        h_point[cpoint][2]=dy; 
        cpoint=-1;        
      }
    }
    if(edittype==5){
      int dx=x;
      int dy=y;
      int p=getpoint(dx,dy);
      if(p>=0){
        if(p==cpoint){
          cpoint=-1; 
        }else if(cpoint==-1){
          cpoint=p; 
        }else{
          for(int i=0;i<128;i++){
            if(h_line[i][0]==1){ 
              if((h_line[i][1]==cpoint && h_line[i][2]==p) || (h_line[i][2]==cpoint && h_line[i][1]==p)){
                 h_line[i][0]=0; 
              }
            }
          }
          cpoint=-1;
        }
      }
    }
  
    if(edittype==9){
      int dx=x;
      int dy=y;
      int p=getpoint(dx,dy);
      if(p>=0){
        if(p==cpoint){
          cpoint=-1; 
        }else if(cpoint==-1){
          cpoint=p; 
        }else{
          int ck=0;
          for(int i=0;i<128;i++){
            if(h_line[i][0]==1){ 
              if((h_line[i][1]==cpoint && h_line[i][2]==p) || (h_line[i][2]==cpoint && h_line[i][1]==p)){
                switch(h_line[i][3]){
                case 0:
                  h_line[i][3]=2;
                  break;
                case 1:
                  h_line[i][3]=3;
                  break;
                case 2:
                  h_line[i][3]=0;
                  break;
                case 3:
                  h_line[i][3]=1;
                  break;
                }
                cpoint=-1;
              }   
              
            }
          } 
        }
      }
    }
    if(edittype==2 || edittype==3){
      int dx=x;
      int dy=y;
      int p=getpoint(dx,dy);
      if(p>=0){
        if(p==cpoint){
          cpoint=-1; 
        }else if(cpoint==-1){
          cpoint=p; 
        }else{
          int ck=0;
          for(int i=0;i<128;i++){
            if(h_line[i][0]==1){ 
              if((h_line[i][1]==cpoint && h_line[i][2]==p) || (h_line[i][2]==cpoint && h_line[i][1]==p)){
                if(edittype==2){
                  h_line[i][3]=0;
                }
                if(edittype==3){
                  h_line[i][1]=cpoint;
                  h_line[i][2]=p;
                  h_line[i][3]=1;
                }
                ck=1;
              }   
            }
          }          
          if(ck==0){
            for(int i=0;i<128;i++){
              if(h_line[i][0]==0){           
                h_line[i][0]=1;
                h_line[i][1]=cpoint;
                h_line[i][2]=p;    
                if(edittype==2){
                  h_line[i][3]=0; 
                } else{
                  h_line[i][3]=1; 
                }   
                ck=1;              
                break; 
              }
            }
          }
          if(ck==0){
            message=11;
            messagetime=120;
          }
          cpoint=-1;
        }         
      }      
    }
    if(edittype==7){
      int dx=x;
      int dy=y;
      int p=getpoint(dx,dy);
      int ng=0;
      if(p>=0){
        if(anscount>0){
          int ltype=0;
          if(h_clear_course[anscount-1]==p){
            ng=1;
          } 
          {
           int ok=0;
            for(int i=0;i<128;i++){
              if(h_line[i][0]==1){    
                 if((h_line[i][3]==0 && h_line[i][1]==p && h_line[i][2]==h_clear_course[anscount-1])||((h_line[i][3]==0 || h_line[i][3]==1) && h_line[i][2]==p && h_line[i][1]==h_clear_course[anscount-1])){
                     ok=1;
                     ltype=0;
                     break;
                 }
                 if((h_line[i][3]==2 && h_line[i][1]==p && h_line[i][2]==h_clear_course[anscount-1])||((h_line[i][3]==2 || h_line[i][3]==3) && h_line[i][2]==p && h_line[i][1]==h_clear_course[anscount-1])){
                     ok=1;
                     ltype=1;
                     break;
                 }
              }
            }       
            if(ok==0) ng=1;
         } 
         {
           int nn=0;
           for(int i=1;i<anscount;i++){
             if( h_clear_course[i-1]==h_clear_course[anscount-1] && h_clear_course[i]==p){
               nn++;  
             }
             if( h_clear_course[i]==h_clear_course[anscount-1] && h_clear_course[i-1]==p){
               nn++;  
             }
           }
           if(ltype==0){
             if(nn>0){
               ng=1; 
             }
           }
           if(ltype==1){
             if(nn>1){
               ng=1; 
             }
           }
         }
        }
        if(ng==0){
           cpoint=p;
           if(anscount<0) anscount=0;
           h_clear_course[anscount]=p;
           anscount++;          
        }
      }
    }
    if(edittype==8){
      int dx=x;
      int dy=y;
      int p=getpoint(dx,dy);
      int ng=0;
      if(p>=0){
        if(anscount>0){
          if(p==cpoint){
            cpoint=-1; 
          }else if(cpoint==-1){
            cpoint=p; 
          }else{            
            for(int i=0;i<anscount-1;i++){
              if(( h_clear_course[i]==cpoint && h_clear_course[i+1]==p ) || ( h_clear_course[i]==p && h_clear_course[i+1]==cpoint )){
                 if(h_clear_quest_course[i]==1){
                   h_clear_quest_course[i]=0;
                 }else{
                   h_clear_quest_course[i]=1; 
                 }
              }              
            }
            cpoint=-1;
          }
        }
      }
    }
  }
  if(step==1){
    int dx=x;
    int dy=y;
    int p=getpoint(dx,dy);
  
    if(p>=0){
      int ok=0;
      if(playpoint>=0){
       for(int i=0;i<128;i++){
         if(h_line[i][0]==1 && (((h_line[i][3]==0 || h_line[i][3]==1) && h_line_ck[i]==0) || ((h_line[i][3]==2 || h_line[i][3]==3) && h_line_ck[i]<2))){    
           if(((h_line[i][3]==0 || h_line[i][3]==2) && h_line[i][1]==p && h_line[i][2]==playpoint)||(h_line[i][2]==p && h_line[i][1]==playpoint)){
             
              if( h_clear_quest_course[tern]==1 ){
                if( h_clear_course[tern]==playpoint && h_clear_course[tern+1]==p){
                  ok=1; 
                  h_line_ck[i]++;
                  h_play[tern]=i;
                }
              }else{             
                ok=1;
                h_line_ck[i]++;
                h_play[tern]=i;
              }
              break;
           }
         }
       }
       if(ok==1){
         playpoint=p;
         int noclear=0; 
         for(int i=0;i<128;i++){
           if(h_line[i][0]==1 && (((h_line[i][3]==0 || h_line[i][3]==1) && h_line_ck[i]==0) || ((h_line[i][3]==2 || h_line[i][3]==3) && h_line_ck[i]<2))){  
             noclear=1;
           }
         }
         if(noclear==0) playclear=1;
         tern++;
       }
      }else{
        playpoint=p;
        
      }       
    }
  }
  
}

public void keyPressed() {
  if(step==0){
    if(key>='0' && key<='9'){
      int num=key-'0';
      if(step==0){
        edittype=num;    
      }
      cpoint=-1;
      if(num==7){
        anscount=-1;
        for(int i=0;i<256;i++){ 
          h_clear_quest_course[i]=0;
        }
      }
  //    fileid=((fileid*10)+num)%10000;    
  //    println(nf(fileid,4));
  //    filenames="hitodata"+nf(fileid,4)+".txt";
    }
  }
  if(step==1){
    if(key>='0' && key<='9'){
      int num=key-'0';
      
      if(num==1){
        tern=0;
        playpoint=-1;
        playclear=0;
        for(int i=0;i<256;i++){
          h_line_ck[i]=0;
          h_play[i]=0;
        } 
      }
    }
    if(key=='B' || key=='b'){
      if(tern>0){
        tern--;
        h_line_ck[h_play[tern]]=0;
        
        if(h_line[h_play[tern]][1]==playpoint){
          playpoint=h_line[h_play[tern]][2];
        }else{
          playpoint=h_line[h_play[tern]][1];
        }
        
        h_play[tern]=0;
        playclear=0;
      }else{
        playpoint=-1;
      }
    }
  }
  if(step==0){
    if(key=='+' || key=='*'){
      for(int i=0;i<64;i++){
        if(h_point[i][0]!=0){
          int vx=h_point[i][1]-500;
          int vy=h_point[i][2]-500;
          if(key=='+'){
            vx=vx*21/20;
            vy=vy*21/20; 
          }else{
            vx=vx*101/100;
            vy=vy*101/100; 
          }
          h_point[i][1]=vx+500;
          h_point[i][2]=vy+500;      
        }
      }
    }
    if(key=='-'){
      for(int i=0;i<64;i++){
        if(h_point[i][0]!=0){
          int vx=h_point[i][1]-500;
          int vy=h_point[i][2]-500;
          vx=vx*20/21;
          vy=vy*20/21;  
          h_point[i][1]=vx+500;
          h_point[i][2]=vy+500;      
        }
      }
    }
    if (key == CODED) {
      int v=glid*25;
      if(v==0) v=1;
      if(keyCode == UP){
          for(int i=0;i<64;i++){
            if(h_point[i][0]!=0){
              h_point[i][2]-=v;
            }
          }
      }
      if(keyCode == DOWN){
          for(int i=0;i<64;i++){
            if(h_point[i][0]!=0){
              h_point[i][2]+=v;
            }
          }
      }
      if(keyCode == LEFT){
          for(int i=0;i<64;i++){
            if(h_point[i][0]!=0){
              h_point[i][1]-=v;
            }
          }
      }
      if(keyCode == RIGHT){
          for(int i=0;i<64;i++){
            if(h_point[i][0]!=0){
              h_point[i][1]+=v;
            }
          }
      }
    }
  }
  if(key=='S' || key=='s'){
    String savePath = "";
    savePath=selectOutput(); 
    if(savePath!=null){
      filenames= savePath;
      Savedata();
    }
  }
  if(key=='L' || key=='l'){
    String loadPath = selectInput(); 
    if(loadPath!=null){
      filenames= loadPath;
      Loaddata();
    }
  }
  if(key=='G' || key=='g'){
     glid=(glid+1)%5;
     if(glid==3) glid=4;
  }
  if(key=='R' || key=='r'){
     glid_lock=(glid_lock+1)%2;
  }
  if(key=='A' || key=='a'){
    anserdisp=(anserdisp+1)%2;
  }
  if(key=='P' || key=='p'){
    step=1;
  }
  if(key=='E' || key=='e'){
    step=0;
  }
 
}



public void Savedata()
{
  int sline=60;
  int maxpoint=10;
  int ll=0;
  String[] strf = new String[1000];  
  strf[ll]="#HITDATA";
  ll++;
  strf[ll]="#VERSION";
  ll++;
  strf[ll]="1";
  ll++;
  strf[ll]="#POINT";
  ll++;
  {
    int usenum=0;
    for(int i=0;i<64;i++){
      if(h_point[i][0]==1){
        strf[ll]=(h_point[i][1])+","+(h_point[i][2]);
        h_point[i][3]=usenum;
        usenum++; 
        ll++;
      }    
    }    
  }
  strf[ll]="#END";
  ll++;
  strf[ll]="#LINE";
  ll++;
  {
    for(int i=0;i<128;i++){
      if(h_line[i][0]==1 && h_line[i][1]!=h_line[i][2]){
        int l1=0,l2=0;
        if(h_point[h_line[i][1]][0]==1 && h_point[h_line[i][2]][0]==1){
          strf[ll]=(h_point[h_line[i][1]][3])+","+(h_point[h_line[i][2]][3])+","+h_line[i][3];
          ll++;
        }
      }    
    }    
  }  
  strf[ll]="#END";
  ll++;
  strf[ll]="#ANSER";
  ll++;
  {
    int ok=0;
    strf[ll]="";
    for(int i=0;i<anscount;i++){
      println("ANS"+i+"  p"+h_clear_course[i]+"  "+ h_point[h_clear_course[i]][0]);
      if(h_clear_course[i]>=0 && h_point[h_clear_course[i]][0]==1){
        strf[ll]+=h_point[h_clear_course[i]][3]+",";
        ok=1;
      }
    }   
   if(ok==1) ll++; 
  }
  strf[ll]="#QUEST";
  ll++;
  {
    int ok=0;
    strf[ll]="";
    for(int i=0;i<anscount;i++){
      println("QUE"+i+"  p"+h_clear_quest_course[i]+"  "+ h_point[h_clear_quest_course[i]][0]);
      if(h_clear_quest_course[i]>=0 && h_point[h_clear_quest_course[i]][0]==1){
        strf[ll]+=h_point[h_clear_quest_course[i]][3]+",";
        ok=1;
      }
    }   
   if(ok==1) ll++; 
  }
  strf[ll]="#END";
  ll++; 
  
  String[] strfs = new String[ll];  
  for(int i=0;i<ll;i++){
    strfs[i]=strf[i]; 
  }
  saveStrings( filenames ,strfs);
  message=1;
  messagetime=120;
  
}

public void Loaddata()
{
  String endstr="#END";
  String lines[] = loadStrings(filenames); 
  int ok=0;
  if(lines==null || lines.length<=0){
    message=3;
    messagetime=120;
    return;
  }
  
  for (int i=0; i < lines.length; i++) {
    println(lines[i]);
  }
  //checkdata
  String ckstr="#HITDATA";
  if(lines[0].equals(ckstr) != true) {
    message=4;
    messagetime=120;
    return;
  }
  //version
  ok=0;
  String ckstr2="#VERSION";
  for (int i=0; i < lines.length; i++) {
     if(lines[i].equals(ckstr2) == true) {
       int ver=parseInt(lines[i+1].replaceAll("[^0-9]",""));
        println("version"+ver);
        if(ver==1){
         
          ok=1;
        }  
        break;
     }else{
       
     }
  }
  if(ok==0){
    message=5;
    messagetime=120;
    return;
  }
  
   for(int i=0;i<64;i++){
    h_point[i][0]=0; 
    h_point[i][1]=0; 
    h_point[i][2]=0;
    h_point[i][3]=0;      
  }
   for(int i=0;i<128;i++){
     h_line[i][0]=0;
   }
   for(int i=0;i<256;i++){
     h_line_ck[i]=0;
     h_play[i]=0;
     h_clear_course[i]=-1;
     h_clear_quest_course[i]=0;
   }
  
  //point
  String ckstr3="#POINT";
  for (int i=0; i < lines.length; i++) {
    if(lines[i].equals(ckstr3) == true) {
      int l=0;
      for (int ii=i+1; ii < lines.length; ii++) {
        if(lines[ii].equals(endstr) == true) {
          break; 
        }
        String s=lines[ii];
        String ls[]=split(s,',');
        int lx=parseInt(ls[0].replaceAll("[^0-9]",""));
        int ly=parseInt(ls[1].replaceAll("[^0-9]",""));
             
        println("POINT x:"+lx+"  y:"+ly);
        
        h_point[l][0]=1;
        h_point[l][1]=lx;
        h_point[l][2]=ly;
        
        l++;
      }            
    }    
  }
  //line
  String ckstr4="#LINE";
  for (int i=0; i < lines.length; i++) {
    if(lines[i].equals(ckstr4) == true) {
      int l=0;
      for (int ii=i+1; ii < lines.length; ii++) {
        if(lines[ii].equals(endstr) == true) {
          break; 
        }
        String s=lines[ii];
        String ls[]=split(s,',');
        int lx=parseInt(ls[0].replaceAll("[^0-9]",""));
        int ly=parseInt(ls[1].replaceAll("[^0-9]",""));
        int type=parseInt(ls[2].replaceAll("[^0-9]",""));
             
        println("LINE x:"+lx+"  y:"+ly+"  type:"+type);
        
        h_line[l][0]=1;
        h_line[l][1]=lx;
        h_line[l][2]=ly;
        h_line[l][3]=type;
        l++;
      }            
    }    
  }
  //clearline
  String ckstr5="#ANSER";
  for (int i=0; i < lines.length; i++) {
    if(lines[i].equals(ckstr5) == true) {
      int l=0;
      {
        int ii=i+1;
        if(lines[ii].equals(endstr) == true) {
          break; 
        }
        String s=lines[ii];
        String ls[]=split(s,',');
        int li=0;
        for(int iii=0;iii<ls.length;iii++){
          if(iii>=256 || li>=256) break;
          int pi=parseInt(ls[iii].replaceAll("[^0-9]",""));
          
          h_clear_course[li]=pi;
          println("ANSER:"+li+":"+pi);
          anscount=li;
          li++;
         
        }               
      }            
    }    
  }
   //questclearline
  String ckstr6="#QUEST";
  for (int i=0; i < lines.length; i++) {
    if(lines[i].equals(ckstr6) == true) {
      int l=0;
      {
        int ii=i+1;
        if(lines[ii].equals(endstr) == true) {
          break; 
        }
        String s=lines[ii];
        String ls[]=split(s,',');
        int li=0;
        for(int iii=0;iii<ls.length;iii++){
          if(iii>=256 || li>=256) break;
          int pi=parseInt(ls[iii].replaceAll("[^0-9]",""));
          
          h_clear_quest_course[li]=pi;
          println("QUEST:"+li+":"+pi);        
          li++;
         
        }               
      }            
    }    
  }
  
  message=2;
  messagetime=120;
}

  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#F0F0F0", "hitohude" });
  }
}
